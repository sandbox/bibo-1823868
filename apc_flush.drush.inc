<?php

/*******************************************************************************
 * DRUSH SUPPORT
 ******************************************************************************/

/**
 * Implementation of hook_drush_command().
 */
function apc_flush_drush_command() {
  $items = array();

  $items['flush-apc'] = array(
    'description' => "Flush APC opcode caches for multiple servers (configured in apc_flush).
                      You can also use drush cc apc",
    'callback'    => 'apc_flush_flush_apc',
    'arguments' => array(),
  );

  return $items;
}


/**
 * Adds a cache clear option for (used by "drush cc".
 */
function apc_flush_drush_cache_clear(&$types) {
  // NOTE: "drush cc all" does not clear ALL caches
  // Read more: http://www.marmelune.net/en/drupal/drush-cc/
  // For that we can however hook into hook_flush_caches().
  
  //$types['all'] = 'apc_flush_drush_wrapper';
  $types['apc'] = 'apc_flush_flush_apc';
}
