<?php
/**
 * @file
 * Administration tasks for the module.
 */
/*


/**
 * Settings form.
 */
function apc_flush_settings() {
  $form = array();

  // Debug settings.
  $form['apc_flush'] = array(
    '#type' => 'fieldset',
    '#title' => t('APC Flush settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );


  $form['apc_flush']['apc_flush_webservers_to_flush'] = array(
    '#type' => 'textfield',
    '#title' => t('Webservers'),
    '#description' => t('<p>Server(s) that run Drupal. Prefix with correct protocol (HTTP), and separate each address with a space.</p>

                          <br/>For example: "http://10.39.8.11 http://10.39.8.12" or "http://web1.mysite.com http://web2.mysite.com". The IPs alone will only work if the Drupal is in the default web_root folder.

                          <p>The server addresses are used to execute synchronized apc flushes. APC opcode caches need
                          to be flushed after code updates on productions servers if apc.stat has been set to 0 for performance.
                          Flushing can be done manually via admin, or on command line via drush ("drush cc all" or "drush cc apc"). </p>
			  <br/><p>!apc_stat_status</p
                          <br/><p>!url_fopen_status</p>
                          <p>!link</p>',
                            array(
                              '!url_fopen_status' => t('"allow_url_fopen" needs to be set to "On" (1) in php.ini or flushing will fail. Current value is: ') .
                                 '<b>' .ini_get('allow_url_fopen') . '</b>.',
                              '!apc_stat_status' => t('APC flushing is only nefessary if "apc.stat" has been set to ""Off" (0) in php.ini. Current value is: ') .
                                 '<b>' .ini_get('apc.stat') . '</b>.',
                              '!link' => l(t('Flush opcode caches on webserver(s)'), 'admin/config/development/do-apc-flush')
                            )
                        ),
    '#default_value' => variable_get('apc_flush_webservers_to_flush', ''),
  );


  $form['apc_flush']['apc_flush_webservers_to_flush_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache flush secret'),
    '#description' => t('Cache flushing flushing can be done without any authorization checks, so it can be used by cron etc.
      However you can also set a secret string, to make sure no one else is flushing your caches.'),
    '#default_value' => variable_get('apc_flush_webservers_to_flush_secret', ''),
  );


  $form['apc_flush']['apc_flush_skip_bootstrap'] = array(
    '#type' => 'checkbox',
    '#title' => t('Skip Drupal bootstrap while flushing'),
    '#description' => t('APC opcode flushing can be executed separately from Drupal. 
     This is fast and efficient, but offers less logging and no anti-flood measures. 
     It is suggested that apc_flush.php is protected from outside traffic, but not internally.

    <p>If the site is in maintenance-mode we will always use skip it, because the site might not be able to respond otherwise.</p>
'),
    '#default_value' => variable_get('apc_flush_skip_bootstrap', 0),
  );


  return system_settings_form($form);
}
