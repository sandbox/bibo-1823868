<?php

/**
 * Implements hook_init().
 */
function apc_flush_init() {
  // This callback is requested as a json callback from anonymous sources.
  // Dont cache it, or the flushing is not triggered.
  if(arg(0) == 'apc-flush'){
    $GLOBALS['conf']['cache'] = FALSE;
  }
}


/**
 * Implements hook_menu().
 */
function apc_flush_menu() {
  // Admin settings
  $items['admin/config/development/apc-flush'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'APC Flush',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('apc_flush_settings'),
    'access arguments' => array('administer apc_flush'),
    'file' => 'apc_flush.admin.inc',
  );

  // Callback that launches synchronized apc opcode flushes.
  $items['admin/config/development/do-apc-flush'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'Flush APC caches',
    'access arguments' => array('administer apc_flush'),
    'page callback' => 'apc_flush_flush_apc',
    'page arguments' => array(),
  );

  // Callback that clears local apc opcode cache.
  $items['apc-flush'] = array(
    'type' => MENU_CALLBACK,
    'title' => 'APC',
    'page callback' => 'apc_flush_flush_apc_local',
    'page arguments' => array(),
    'access callback' => TRUE,
     // 'access callback' => TRUE, // Accessible to anonymous. Protected by secret string, if needed
  );

  return $items;
}



/**
 * Implements hook_permission().
 */
function apc_flush_permission() {
  return array(
    'administer apc_flush' =>  array(
      'title' => t('Administer apc flush'),
    ),
  );
}


/**
 * Implements hook_cron().
 */
//function apc_flush_cron() {
   // Do additional apc flushing during cron?
   // --> currently doesnt really make sense.
//}


/*
 * Apc opcode caches need to be flushed separately on each server running drupal.
 * We can make it automated (cron) and easy for manual flushes with drush / drupal admin.
 * TODO: Hook this into the cache flush of admin_menu's next version. See http://drupal.org/node/588518
 */
function apc_flush_flush_apc($goto = TRUE){
  $servers = check_plain(variable_get('apc_flush_webservers_to_flush', ''));
  $server_array = explode(' ', $servers);
  $caller = php_sapi_name();
  $secret = check_plain(variable_get('apc_flush_webservers_to_flush_secret', ''));
  $return = FALSE;
  global $base_url;

  if($goto && $caller != 'cli') $return = TRUE; // TRUE = will return the use to the admin page.

  // We can fallback to $base_url if we want things to work for single server setups out of the box.
  // NOTE: if $base_url is not defined, drush might set it to http://default, which is unuseable.
  // TODO: add admin checkbox for option to use this fallback?
  if(empty($servers) && $base_url && $base_url != "http://default") $server_array = array($base_url);

  // Only continue if apc is enabled. CLI is often different from actual server.
  if($caller != 'cli' && !ini_get('apc.enabled')){
     drupal_set_message(t('APC is not enabled. You probably want to enable it to get a big free performance boost! Enabling it for php-cli might be problematic though.'));
     if($return){
       drupal_goto('admin/config/development/apc-flush');
     }else{
       return;
     }
  }

  if(variable_get('apc_flush_skip_bootstrap', 0) || variable_get('maintenance_mode', 0)){
    // We might want to use the separate apc_flush.php file to avoid any "heavy" Drupal bootstrapping.
    // This is often required is the site is in maintenance_mode, as the normal callback is likely not responding.
    // This file could be protected from outside traffic in varnish/webserver config, allowing only internal requests.
    $flush_url = base_path(). drupal_get_path('module','apc_flush') . '/apc_flush.php?ts=' . REQUEST_TIME;
    // Downside: cant use secret or logging since there is no db connection and we want to keep this simple.
  }else{
    $flush_url = '/apc-flush?s=' . $secret . '&ts=' . REQUEST_TIME;
 }

 if(empty($server_array)){
   watchdog('apc_flush', t('Couldnt clear APC opcode caches (no servers defined).'));
   drupal_set_message('APC caches were not flushed. You need to define the targeted webservers first.');
 }else{
    // Sending separate webrequest and message for each server. Low timeouts.
    // $conf['http_request_timeout'] = '6';
    $ctx = stream_context_create(array(
      'http' => array(
         'timeout' => 6
         )
      )
    );

    // Ideas stolen from:
    //  - http://stackoverflow.com/questions/911158/how-to-clear-apc-cache-entries
    //  - http://stackoverflow.com/questions/3689371/php-file-get-contents-ignoring-timeout
    foreach($server_array as $key => $server){
     $server = trim($server);

     if(!$server || !valid_url($server, TRUE)){
       // No separate server(s) defined? Lets just clear this current one.
       // Clearing is fine if its a single server, but halfcleared multiservers are not what you want.
       if($caller != 'cli' && apc_clear_cache()){
         drupal_set_message(t('APC flush: There were no web servers defined, cleared current server.'));

         if($return){
           drupal_goto('admin/config/development/apc-flush');
         }else{
           return;
         }
       }
     }

      // The key param is added to the url just so that we skip possible page caching (ts is not enough).
      $result = json_decode(file_get_contents(trim($server) . $flush_url . '-' . $key, 0, $ctx), true);
      if($result['result'] == 'success'){
        watchdog('apc_flush', t('Cleared APC opcode caches for !servers', array('!servers' => $servers)));
        drupal_set_message(t('Successfully cleared APC opcode caches on: !server.', array('!server' => $server)));
      }else{
        watchdog('apc_flush', t('FAILED to clear APC opcode caches for !server', array('!server' => $server)));
        drupal_set_message(t('Sorry! Failed to clear APC opcode caches on !server.', array('!server' => $server)));
      }
    }

    // Redirect back to ruutu-light admin (if not called by drush/cli). Dont redirect if launched via global cache flush.
    if($return){
      drupal_goto('admin/config/development/apc-flush');
    }else{
      return;
    }
  }
}


/*
* This function is called via a separate HTTP-request, initiated by drupal/drush.
* --> Todo: push out of Drupal and make it work
*/
function apc_flush_flush_apc_local(){
  // Disable Drupal + Varnish caching for this request.
  // Caching is able being cimcumvented by adding bogus parameters (ts).
  $GLOBALS['conf']['cache'] = FALSE;

  // Cache clearing is top secret stuff - if you want it to be!
  $secret = variable_get('apc_flush_webservers_to_flush_secret', '');
  $set_secret = isset($_GET['s']) ? check_plain($_GET['s']) : '';

  if($secret && $secret != $set_secret || !function_exists('apc_clear_cache')){
    watchdog('apc_flush', t('Server: !server FAILED its APC opcode cache flush.', array('!server' => $_SERVER['SERVER_ADDR'])));
    echo json_encode(array('result' => 'failure'));
    ajax_footer();
  }else{
    // Clear cache and inform the original requester.
    watchdog('apc_flush', t('Server: !server SUCCESSFULLY had its APC opcode cache flushed.', array('!server' => $_SERVER['SERVER_ADDR'])));

    //apc_clear_cache('user'); // user-caching is currently unused.
    if(apc_clear_cache()){
      echo json_encode(array('result' => 'success'));
    }
    ajax_footer();
  }
}


/*
 * Implementation of hook flush caches
 *
 * This is called by drupal_flush_all_caches(), which is used by "drush cc all",
 * (http://api.drupal.org/api/drupal/includes%21common.inc/function/drupal_flush_all_caches/7)
 * This is the most direct way to hook into that.
 */
function apc_flush_flush_caches() {
  apc_flush_flush_apc(FALSE);

  // Normally this hook is expected to return cache_table -names as an array,
  // but we do the flushing ourselves.
  return array();
}
