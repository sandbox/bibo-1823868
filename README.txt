This text is copypaste from: http://drupal.org/project/apc_flush
------------------------------------------------------------------

This module is intended to be used as an APC opcode cache flush automator for high performance Drupal-setups.

If you have setup PHP with apc.stat=0 in production and you deploy code updates (with version control or CI or whatever) you need to either restart your webserver or restart php-fpm to flush the opcode cache. You could also execute apc_clear_cache() with a script.

This task becomes more complicated when you have multiple webfronts. All servers need to flush in sync - or they will execute different code, which will mess up everything. Also, doing webserver restarts theorically might abrupt current http requests, while apc_clear_cache() doesn't.

How to use
This module integrates with standard Drupal cache clear and drush. To flush caches (for one or more servers) with drush, you can run:
drush cc all or drush cc apc


When to use
This module is useless for you, unless you have configured your production PHP server(s) with apc.stat=0!

Code deployments to production should usually/always end up with a global cache clear, and the cache clear integration makes things work mostly out of the box. There are some special cases such as when the site is in maintenance mode, but those challenges can be circumvented.


Why to use
Using apc.stat=0 is sometimes benchmarked at about 6% faster page load times and similar load improvement, but this becomes increasingly useful if disk I/O is an issue, especially when the code is shared with nfs.

More information about apc.stat, stolen from php.net manual:

Be careful changing this setting. This defaults to on, forcing APC to stat (check) the script on each request to determine if it has been modified. If it has been modified it will recompile and cache the new version. If this setting is off, APC will not check, which usually means that to force APC to recheck files, the web server will have to be restarted or the cache will have to be manually cleared. Note that FastCGI web server configurations may not clear the cache on restart. On a production server where the script files rarely change, a significant performance boost can be achieved by disabled stats.

For included/required files this option applies as well, but note that for relative path includes (any path that doesn't start with / on Unix) APC has to check in order to uniquely identify the file. If you use absolute path includes APC can skip the stat and use that absolute path as the unique identifier for the file.

This module also clears APC's user cache, not only the opcode that is affected by apc.stat=0..


PHP-CLI vs php-fpm etc
Please also note that PHP-cli (command line PHP) APC is separate from the standard PHP, so flushing APC on PHP-cli doesn't affect the opcode cache used by the webserver. PHP-cli apc.stat=0 or apc.enable_cli=1 don't need any cache flushing, because the cache is actually not kept for subsequent PHP-cli runs. See more info:
* http://stackoverflow.com/questions/1245242/php-apc-in-cli-mode
* http://drupal.org/node/1051748


Requirements
You should first make sure that "allow_url_fopen" is set to "On" (1) in php.ini. Without it flushing multiple servers/with drush will fail.


Setup
* Single server setups should work without any Drupal configuration if you've defined $base_url in settings.php.

* Multi server setups require you to define each web servers (internal network) address at "admin/config/development/apc-flush". This could be for example: : "http://10.39.8.11 http://10.39.8.12" or "http://web1.mysite.com http://web2.mysite.com".

You would probably want to keep those subdomains in each server's /etc/hosts file, and unavailable from outside the production network.


Sponsors
This module was sponsored by Nelonen Media and developed by Wunderkraut.
