<?php
/*
* This file is an alternative to running Drupal and apc_flush_flush_apc_local().
* A separate file is a bit lighter, which has might matter if cache-flushing is initiated all the time,
* or if someone is hammering the flushing mechanism. For that, a "secret" token can be used with the
* Drupalized version, but this separate flushing script doesnt support that. Also, the logging has to happen separately.
*
* This script is called via a separate HTTP-request, that is initiated by drupal/drush.
*/

// DO NOT CACHE
header('Cache-Control: no-cache, must-revalidate');

if(ini_get('apc.enabled')){
  // Try to flush APC opcodecaches
  if(apc_clear_cache()){
    // .. and tell the caller we succeeded
    echo json_encode(array('result' => 'success'));
  }else{
    echo json_encode(array('result' => 'failure'));
  }
}else{
    //  apc.stat = 1 is the default and opcode doesnt require manual clearing.
    // (Do we really care? This is not necessarly a problem) -> We want KISS.
    // echo json_encode(array('result' => 'unneeded'));
    echo json_encode(array('result' => 'success'));
}

// THE END
die();
